var express = require('express');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'iSurvey-image' });
});

var upload = multer({
  dest: 'public/uploads'
});

var type = upload.single('recfile');

/* Upload images */
router.post('/upload', type, function(req, res, next) {
  var image = 'public/uploads/' + req.file.originalname;
  fs.readFile(req.file.path, function(err, data) {
    fs.writeFile(image, data, function(err) {
      if(err) {
        console.log(err);
        res.send(500);
      }

      else {
        fs.unlink(req.file.path);
        //res.render('picture', {
          //title: 'iSurvey-image',
          //image_src: '/uploads/' + req.file.originalname
        //});
        var out = {
          image_src: '/uploads/' + req.file.originalname,
          success:true
        }
         res.json(out)
      }
    });
  });
});

module.exports = router;
